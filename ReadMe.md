## Controls

```
Arrow Keys for movements
Attack : Z
```

## Asset Credits
1. [Stealthix Animated Fires](https://stealthix.itch.io/animated-fires?download)
2. [Big Buck Bunny Tree Asset](https://bigbuckbunny.itch.io/trees-assets-pack)
3. [Foxlybr Asset Tileset Platformer Grass](https://foxlybr.itch.io/23421124244)
4. [Asset SFX](http://devassets.com/assets/2d-mega-pack/)
5. [Asset Adventurer and Imp Credit](https://elthen.itch.io/)
