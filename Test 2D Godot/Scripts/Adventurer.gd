extends KinematicBody2D

export (int) var run_speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

var state_machine
var attacks = ["attack 1", "attack 2", "attack 3"]
var velocity = Vector2()
const UP = Vector2(0,-1)

var is_attacking = false
var atk_timer = 0

func _ready():
	state_machine = $AnimationTree.get("parameters/playback")
	
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input(delta)
	velocity = move_and_slide(velocity, UP)

func die():
	$Timer.start()
	state_machine.travel("die")
	$Die.play()
	set_physics_process(false)
	

func get_input(delta):
	var current = state_machine.get_current_node()
	if Input.is_action_just_pressed("ui_attack"):
		is_attacking = true
		state_machine.travel(attacks[randi() % 3])
		return
	
	if is_attacking:
		atk_timer += delta
		
	if atk_timer >= 0.4:
		is_attacking = false
		atk_timer = 0
	
	if Input.is_action_pressed('ui_right') :
		$Sprite.scale.x = 1
		velocity.x = run_speed
	elif Input.is_action_pressed('ui_left'):
		$Sprite.scale.x = -1
		velocity.x = -run_speed
	else:
		velocity.x = 0	
		
	if Input.is_action_just_pressed("ui_up"):
		print(str(get_floor_velocity().y) + str(delta))
		if is_on_floor():
			if get_floor_velocity().y < 0:
				print("Masuk sini")
				velocity.y = get_floor_velocity().y + jump_speed
			else:
				velocity.y = jump_speed
			
		
	if velocity.x != 0 and !is_attacking:
		state_machine.travel("run")
		
	if velocity.x == 0 and !is_attacking:
		state_machine.travel("idle")
		
	#if get_slide_count() > 0:
	#	for i in range(get_slide_count()):
	#		if "Enemy" in get_slide_collision(i).collider.name:
	#			die()

func _on_SwordHit_body_entered(body):
	print("Body is: " + str(body.name))
	if body.name.substr(0,5) == "Enemy":
		body.take_damage(10)
	pass # Replace with function body.


func _on_Timer_timeout():
	get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
	pass # Replace with function body.
