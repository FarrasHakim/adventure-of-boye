extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()
var jump_count = 0

var stateMachine
	
func _ready():
		stateMachine = $AnimationTree.get("parameters/playback")
		stateMachine.start("Player_Idle")
		
		
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	var current_state = stateMachine.get_current_node()
	
	#print("Current Node: " + str(current_state))
	if Input.is_action_pressed('ui_right') :
		$Sprite.scale.x = 1
		velocity.x = speed
	elif Input.is_action_pressed('ui_left'):
		$Sprite.scale.x = -1
		velocity.x = -speed
	else:
		velocity.x = 0	
		
	if Input.is_action_just_pressed('ui_up'):		
		if is_on_floor():
			velocity.y = jump_speed
			stateMachine.travel("Player_Jump")
			return
			
			
	if velocity.x == 0 and is_on_floor():
		stateMachine.travel("Player_Idle")
	
	
	
	velocity = move_and_slide(velocity, UP)
	

		