extends KinematicBody2D

const GRAVITY_VEC = Vector2(0, 900)
const FLOOR_NORMAL = Vector2(0, -1)

export (int) var WALK_SPEED = 70
const STATE_WALKING = 0
const STATE_KILLED = 1

var linear_velocity = Vector2()
var direction = 1
var anim=""
var health = 30
var is_dead = false

var state = STATE_WALKING

onready var detect_floor = $Sprite/detect_floor
onready var detect_wall = $Sprite/detect_wall
onready var sprite = $Sprite
onready var fireBall = preload("res://Scenes/Fireball.tscn")
var new_anim = "idle"

func _ready():
	pass

func _physics_process(delta):
	
	if !is_dead:
		
	
		if state==STATE_WALKING:
			linear_velocity += GRAVITY_VEC * delta
			linear_velocity.x = direction * WALK_SPEED
			linear_velocity = move_and_slide(linear_velocity, FLOOR_NORMAL)

			if not detect_floor.is_colliding() or detect_wall.is_colliding():
				#print("Floor : " + str(detect_floor.is_colliding()))
				#print("Wall : " + str(detect_wall.is_colliding()))
				direction *= -1

			sprite.scale = Vector2(direction, 1.0)
			new_anim = "run"
	
		if health <= 0:
			$Timer.start()
			is_dead = true
			new_anim = "die"
			$CollisionShape2D.disabled = true
			
		if anim != new_anim:
			anim = new_anim
			sprite.play(anim)


func _on_Area2D_body_entered(body):
	if !is_dead:
		if body.name == "Player":
			summon_fireball()

func summon_fireball():
	var newfireBall = fireBall.instance()
	if sprite.scale.x == 1:
			newfireBall.set_fireball_dir(-1)
	get_parent().call_deferred("add_child", newfireBall);
	newfireBall.position = $Sprite/Position2D.global_position
	
func take_damage(damage):
	$Hit.play()
	$Ouch.play()
	health -= damage
	print("Taking "  + str(damage) + " damage")

func _on_Timer_timeout():
	call_deferred("free")
	pass # Replace with function body.
