extends Node2D

func _ready():
	$Player/AudioStreamPlayer2D.play()


func _on_FallDeath_body_entered(body):
	if body.name == "Player":
		body.die()


func _on_NextStage_body_entered(body):
	if body.name == "Player":
		get_tree().change_scene(str("res://Scenes/Credits.tscn"))
