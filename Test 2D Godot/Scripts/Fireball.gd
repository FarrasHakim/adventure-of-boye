extends AnimatedSprite

const SPEED = -200
var velocity = Vector2()
var direction = 1

func _ready():
	pass 
	
func set_fireball_dir(dir):
	direction = dir
	if dir == -1:
		scale.x = -0.2

func _physics_process(delta):
	velocity.x = SPEED * delta * direction
	translate(velocity)

func _on_VisibilityNotifier2D_screen_exited():
	call_deferred("free")

func _on_KinematicBody2D_body_entered(body):	
	if body.name == "Player":
		body.die()	
	if body.name.substr(0,5) != "Enemy":
		call_deferred("free")
