extends KinematicBody2D

export (int) var shoot_cooldown = 5

var health = 50
var is_dead = false
var is_attacking = false
var atk_timer = 0
var anim=""
var new_anim = "idle"
var shoot_timer = 0
var can_shoot = false

onready var sprite = $Sprite
onready var shootPos1 = $Position2D
onready var shootPos2 = $Position2D2
onready var shootPos3 = $Position2D3
onready var fireBall = preload("res://Scenes/GreenFire.tscn")

func _ready():
	pass

func _physics_process(delta):
	if !is_dead:
		if !is_attacking:
			new_anim = "idle"
		
		if is_attacking:			
			atk_timer += delta
		
		if atk_timer >= 1:
			is_attacking = false
			atk_timer = 0
			pass
		if shoot_timer >= shoot_cooldown && can_shoot:
			summon_fireball()
			
		if health <= 0:
			$Timer.start()
			is_dead = true
			new_anim = "die"
			$CollisionShape2D.disabled = true
			
		if anim != new_anim:
			anim = new_anim
			sprite.play(anim)
		
		shoot_timer += delta

func summon_fireball():
	shoot_timer = 0
	is_attacking = true
	new_anim = "attack"
	var newfireBall = fireBall.instance()
	get_parent().call_deferred("add_child", newfireBall);
	var num = randi()%3+1
	if num == 1:
		newfireBall.position = shootPos1.global_position
	elif num == 2:
		newfireBall.position = shootPos2.global_position
	else:
		newfireBall.position = shootPos3.global_position
	
func take_damage(damage):
	$Hit.play()
	$Ouch.play()
	health -= damage
	print("Taking "  + str(damage) + " damage")

func _on_Timer_timeout():
	call_deferred("free")

func _on_PlayerDetect_body_entered(body):
	if body.name == "Player":
		can_shoot = true
	 # Replace with function body.


func _on_PlayerDetect_body_exited(body):
	if body.name == "Player":
		can_shoot = false
